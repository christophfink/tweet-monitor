	DROP TABLE bangkokshutdown_users;
	CREATE TABLE IF NOT EXISTS bangkokshutdown_users (
		row SERIAL PRIMARY KEY,
		created_at TIMESTAMP WITH TIME ZONE,
		description CHARACTER VARYING (512),
		favourites_count INTEGER,
		followers_count INTEGER,
		friends_count INTEGER,
		geo_enabled BOOLEAN,
		id BIGINT,
		lang CHARACTER VARYING (16),
		listed_count INTEGER,
		location CHARACTER VARYING (255),
		name CHARACTER VARYING (512),
		protected BOOLEAN,
		screen_name CHARACTER VARYING (255),
		statuses_count INTEGER,
		time_zone CHARACTER VARYING (64),
		utc_offset INTEGER
	);
        CREATE INDEX ON bangkokshutdown_users USING BTREE(screen_name);
        CREATE INDEX ON bangkokshutdown_users USING BTREE(id);

	DROP TABLE bangkokshutdown_tweets;
        CREATE TABLE IF NOT EXISTS bangkokshutdown_tweets (
                row SERIAL PRIMARY KEY, 
		created_at TIMESTAMP WITH TIME ZONE,
		favorited BOOLEAN,
		favorite_count INTEGER,
		id BIGINT,
		in_reply_to_user_id BIGINT,
		in_reply_to_status_id BIGINT,
		lang CHARACTER VARYING (16),
		place CHARACTER VARYING (255),
		retweet_count INTEGER,
		source CHARACTER VARYING (255),
		text CHARACTER VARYING (160),
		truncated BOOLEAN,
		location CHARACTER VARYING (255),
		usr BIGINT,
		urls CHARACTER VARYING (1024) [],
		user_mentions BIGINT[],
		hashtags CHARACTER VARYING (255) [],
                geom geometry(POINT,4326)
        );
        CREATE INDEX ON bangkokshutdown_tweets USING GIST(geom);
        CREATE INDEX ON bangkokshutdown_tweets USING BTREE(id);
