#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#
#
#/***************************************************************************
#	Tweet-Monitor
#
#	A script to constantly monitor twitter for short messages corresponding
#	to a configurable filter
#
#			      -------------------
#	begin		: 2014-01-13
#	copyright	    : (C) 2014 by Christoph Fink
#	email		: christoph.fink@gmail.com
# ***************************************************************************/
#
#/***************************************************************************
# *									 *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.				   *
# *									 *
# ***************************************************************************/

from __future__ import print_function
import psycopg2, time, locale, fileinput,json,re
from datetime import datetime
from dateutil import parser
from urllib import quote

from tokens import consumer_key, consumer_secret, access_token_key, access_token_secret
from pg_conn import pg_conn # PG connection string

locale.setlocale(locale.LC_ALL, 'C') # for datetime-parsing

searchterm=u'ปิดกทม OR ปิดกรุงเทพ OR หมดความชอบธรรม OR ปิดเพื่อเปลี่ยน OR bangkokprotest OR bangkokprotests OR bangkokshutdown OR BKKShudown OR bkkshutdown OR exitTH OR iPray4BKK OR jan13bkk OR january13 OR occupybangkok OR ocupybangkok OR phuketshutdown OR PrayForThailand OR praying4Thailand OR redshirt OR reformbeforeelection OR RespectMyVote OR restartthailand OR ShutdownBangkok OR shutdownbkk OR shutdownBKKshutdown OR ThaiShame OR thaiuprising OR YalaShutdown OR yellowshirt'

extent=(99,12,103,15) #minx,miny,maxx,maxy

table_tweets="bangkokshutdown_tweets"
table_users="bangkokshutdown_users"

create_sql="""
	CREATE TABLE IF NOT EXISTS %(table_users)s (
		row SERIAL PRIMARY KEY,
		created_at TIMESTAMP WITH TIME ZONE,
		description CHARACTER VARYING (512),
		favourites_count INTEGER,
		followers_count INTEGER,
		friends_count INTEGER,
		geo_enabled BOOLEAN,
		id BIGINT,
		lang CHARACTER VARYING (16),
		listed_count INTEGER,
		location CHARACTER VARYING (255),
		name CHARACTER VARYING (512),
		protected BOOLEAN,
		screen_name CHARACTER VARYING (255),
		statuses_count INTEGER,
		time_zone CHARACTER VARYING (64),
		utc_offset INTEGER
	);
	CREATE INDEX ON %(table_users)s USING BTREE(screen_name);
	CREATE INDEX ON %(table_users)s USING BTREE(id);

	CREATE TABLE IF NOT EXISTS %(table_tweets)s (
		row SERIAL PRIMARY KEY,
		created_at TIMESTAMP WITH TIME ZONE,
		favorited BOOLEAN,
		favorite_count INTEGER,
		id BIGINT,
		in_reply_to_user_id BIGINT,
		in_reply_to_status_id BIGINT,
		lang CHARACTER VARYING (16),
		place CHARACTER VARYING (255),
		retweet_count INTEGER,
		source CHARACTER VARYING (255),
		text CHARACTER VARYING (160),
		truncated BOOLEAN,
		location CHARACTER VARYING (255),
		usr BIGINT,
		urls CHARACTER VARYING (1024) [],
		user_mentions BIGINT[],
		hashtags CHARACTER VARYING (255) [],
		geom geometry(POINT,4326)
	);
	CREATE INDEX ON %(table_tweets)s USING GIST(geom);
	CREATE INDEX ON %(table_tweets)s USING BTREE(id);
""" %({"table_users":table_users,"table_tweets":table_tweets})




def process_tweet(tweet,cursor):
	"""
	inserts a tweet into the database, 
	also creates/updates user record etc
	"""
	try:
		#created_at=datetime.fromtimestamp(tweet.created_at_in_seconds) 
		created_at=parser.parse(tweet["created_at"])
		urls=[u["expanded_url"][:1023] for u in tweet["urls"]]
		user_mentions=[u["id"] for u in tweet["user_mentions"]]
		hashtags=[h["text"][:254] for h in tweet["hashtags"]]
		try:
			lon=tweet["coordinates"]['coordinates'][0]
			lat=tweet["coordinates"]['coordinates'][1]
		except:
			lon=None
			lat=None

		cursor.execute("SELECT count(*)>0 FROM "+table_tweets+" WHERE id=%s", (tweet["id"],))
		if cursor.fetchone()[0]>0:
			return False # we already saved that tweet at some point, don't produce duplicates on purpose
	
		cursor.execute("SELECT nextval(pg_get_serial_sequence('"+table_tweets+"', 'row'))")
		row=cursor.fetchone()[0]
	
		cursor.execute("\
			INSERT INTO "+table_tweets+" \
			VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, ST_SetSRID(ST_Point(%s, %s),4326))",
			(
				row, 
				created_at, 
				tweet["favorited"], 
				tweet["favorite_count"], 
				tweet["id"], 
				tweet["in_reply_to_user_id"], 
				tweet["in_reply_to_status_id"], 
				tweet["lang"][:15] if tweet["lang"] is not None else "",
				str(tweet["place"])[:254] if tweet["place"] is not None else "",
				tweet["retweet_count"], 
				tweet["source"][:254] if tweet["source"] is not None else "",
				tweet["text"][:159] if tweet["text"] is not None else "",
				tweet["truncated"], 
				tweet["location"][:254] if tweet["location"] is not None else "", 
				tweet["user"]["id"], 
				urls, 
				user_mentions, 
				hashtags, 
				lon, lat
			)
		)
	
		cursor.execute("SELECT count(*)>0 FROM "+table_users+" WHERE id=%s", (tweet["user"]["id"],))
		if cursor.fetchone()[0]==0: # user does not exist already -> insert them
			#created_at=datetime.strptime(tweet.user.created_at,"%a %b %d %H:%M:%S %z %Y") # does not work with tz -> http://stackoverflow.com/questions/10494312/parsing-time-string-in-python#10494427
			created_at=parser.parse(tweet["user"]["created_at"])			
			
			cursor.execute("SELECT nextval(pg_get_serial_sequence('"+table_users+"', 'row'))")
			row=cursor.fetchone()[0]
			
			cursor.execute("\
				INSERT INTO "+table_users+" \
				VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
				(
					row, 
					created_at, 
					tweet["user"]["description"][:511] if tweet["user"]["description"] is not None else "", 
					tweet["user"]["favourites_count"], 
					tweet["user"]["followers_count"], 
					tweet["user"]["friends_count"], 
					tweet["user"]["geo_enabled"], 
					tweet["user"]["id"], 
					tweet["user"]["lang"][:15] if tweet["user"]["lang"] is not None else "", 
					tweet["user"]["listed_count"], 
					tweet["user"]["location"][:254] if tweet["user"]["location"] is not None else "", 
					tweet["user"]["name"][:511] if tweet["user"]["name"] is not None else "", 
					tweet["user"]["protected"], 
					tweet["user"]["screen_name"][:254] if tweet["user"]["screen_name"] is not None else "", 
					tweet["user"]["statuses_count"], 
					tweet["user"]["time_zone"][:63] if tweet["user"]["time_zone"] is not None else "", 
					tweet["user"]["utc_offset"]
				)
			)

	except Exception,e:
		print(str(e))
		print("Could not insert tweet #%d into DB" %(tweet["id"],))
		print(tweet)
		return False
	return True

def main():
	total=0
	geo=0
	within=0
	successful=0
	#searchterms=[s.lower() for s in searchterm.split(u' OR ')]
	try:
		connection=psycopg2.connect(pg_conn)
		cursor=connection.cursor()
		cursor.execute("SELECT count(*)=2 FROM information_schema.tables WHERE table_name=%s OR table_name=%s", (table_tweets,table_users))
		if not cursor.fetchone()[0]:
			cursor.execute(createsql)
	except:
		print("Database connection could not be established. Exiting.")
		return -1

	for line in fileinput.input():
		total+=1
		try:
			tweet={
				"created_at": datetime.fromtimestamp(0).strftime("%a %b %d %H:%M:%S +0000 %Y"),
				"favorited": False,
				"favorite_count": 0,
				"id":-1,
				"in_reply_to_user_id":-1,
				"in_reply_to_status_id":-1,
				"lang":"",
				"place":"",
				"retweet_count":0,
				"source":"",
				"text":"",
				"truncated":False,
				"location":"",
				"user":{
					"created_at":datetime.fromtimestamp(0).strftime("%a %b %d %H:%M:%S +0000 %Y"),
					"description":"",
					"favourites_count":0,
					"followers_count":0,
					"friends_count":0,
					"geo_enabled":False,
					"id":-1,
					"lang":"",
					"listed_count":0,
					"location":"",
					"name":"",
					"protected":False,
					"screen_name":"",
					"statuses_count":0,
					"time_zone":"",
					"utc_offset":0
				},
				"urls":[],
				"user_mentions":[],
				"hashtags":[],
				"coordinates":(None,None)
			}
	
			try:
				tweet.update(json.loads(line))
				
				# a few tweaks because json was fetched with different ns:
				tweet["urls"]=tweet["entities"]["urls"] if "entities" in tweet else []
				tweet["user_mentions"]=tweet["entities"]["user_mentions"] if "entities" in tweet else []
				tweet["hashtags"]=[{"text":h[1:]} for h in re.findall('# {0,1}[^ ^#]+',tweet["text"])]
				#for h in tweet["hashtags"]:
				#	if h["text"].lower() in searchterms:
				#		if process_tweet(tweet, cursor):
				#			successful+=1
				#		count+=1
				#		print("processed %d/%d tweets" %(successful,count),end='\r')
				#		raise(ContinueParentLoop)
				#for s in searchterms:
				#	if s.lower() in tweet["text"]:
				#		if process_tweet(tweet, cursor):
				#			successful+=1
				#		count+=1
				#		print("processed %d/%d tweets" %(successful,count),end='\r')
				#		raise(ContinueParentLoop)
				try:
					lon=tweet["coordinates"]['coordinates'][0]
					lat=tweet["coordinates"]['coordinates'][1]
					lon=float(lon) if lon is not None else None
					lat=float(lat) if lat is not None else None
					geo+=1
				except:
					lon=None
					lat=None
				if lon is not None and lat is not None:
					if extent[0] < lon < extent[2] and extent[1] < lat < extent[3]:
						within+=1
						if process_tweet(tweet,cursor):
							successful+=1
						print("processed %d/%d/%d/%d tweets" %(successful,within,geo,total),end='\r')
						raise(ContinueParentLoop)
			except ContinueParentLoop:
				continue
			#except Exception,e:
			#	print(str(e))
			#	continue
		
		except KeyboardInterrupt:
			connection.rollback() # otherwise we might end up with gaps (hard to detect!) -> better re-fetch during next run
			print("\n", end='')
			return
	
	connection.commit()
	connection.close()
	print("\n",end='')

class ContinueParentLoop(Exception):
	pass


if __name__ == '__main__':
	main()
