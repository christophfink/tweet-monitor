#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#
#
#/***************************************************************************
#	Tweet-Monitor
#
#	A script to constantly monitor twitter for short messages corresponding
#	to a configurable filter
#
#			      -------------------
#	begin		: 2014-01-13
#	copyright	    : (C) 2014 by Christoph Fink
#	email		: christoph.fink@gmail.com
# ***************************************************************************/
#
#/***************************************************************************
# *									 *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.				   *
# *									 *
# ***************************************************************************/

from __future__ import print_function
import twitter, psycopg2, time, locale
from datetime import datetime
from dateutil import parser
from urllib import quote
from random import random

from tokens import consumer_key, consumer_secret, access_token_key, access_token_secret
from pg_conn import pg_conn # PG connection string

locale.setlocale(locale.LC_ALL, 'C') # for datetime-parsing

table_tweets="bangkokshutdown_tweets"
table_users="bangkokshutdown_users"

searchterm=u"ปิดกทม OR ปิดกรุงเทพ OR หมดความชอบธรรม OR ปิดเพื่อเปลี่ยน OR bangkokprotest OR bangkokprotests OR bangkokshutdown OR BKKShudown OR bkkshutdown OR exitTH OR iPray4BKK OR jan13bkk OR january13 OR occupybangkok OR ocupybangkok OR phuketshutdown OR PrayForThailand OR praying4Thailand OR redshirt OR reformbeforeelection OR RespectMyVote OR restartthailand OR ShutdownBangkok OR shutdownbkk OR shutdownBKKshutdown OR ThaiShame OR thaiuprising OR YalaShutdown OR yellowshirt" 

# OR YoucandoitSuthep OR Yingluck OR whiteshirt OR yingluckshinawatra OR wakeupthailand OR voteforthailand OR VoteFeb2nd2014 OR Vote2Feb OR VoteFeb2 OR thaiuprasing OR thaiupraising OR thaiprotests OR thailandprotest OR thailanduprising OR Suthep OR SuthepThaugsaban OR SuthepThaugsuban"
#searchterm=u"ปิดกทม OR ปิดกรุงเทพ OR หมดความชอบธรรม OR ปิดเพื่อเปลี่ยน OR bangkokprotest OR bangkokshutdown OR bkkshutdown OR jan13bkk occupybangkok OR phuketshutdown OR redshirt OR reformbeforeelection OR RespectMyVote OR restartthailand OR ShutdownBangkok OR shutdownbkk OR thaiuprising OR yellowshirt" # OR YoucandoitSuthep OR Yingluck OR whiteshirt OR yingluckshinawatra OR wakeupthailand OR voteforthailand OR VoteFeb2nd2014 OR Vote2Feb OR VoteFeb2 OR thaiuprasing OR thaiupraising OR thaiprotests OR thailandprotest OR thailanduprising OR Suthep OR SuthepThaugsaban OR SuthepThaugsuban"
#searchterm="#BangkokShutdown"

waittime=15
batchsize=100

create_sql="""
	CREATE TABLE IF NOT EXISTS %(table_users)s (
		row SERIAL PRIMARY KEY,
		created_at TIMESTAMP WITH TIME ZONE,
		description CHARACTER VARYING (512),
		favourites_count INTEGER,
		followers_count INTEGER,
		friends_count INTEGER,
		geo_enabled BOOLEAN,
		id BIGINT,
		lang CHARACTER VARYING (16),
		listed_count INTEGER,
		location CHARACTER VARYING (255),
		name CHARACTER VARYING (512),
		protected BOOLEAN,
		screen_name CHARACTER VARYING (255),
		statuses_count INTEGER,
		time_zone CHARACTER VARYING (64),
		utc_offset INTEGER
	);
	CREATE INDEX ON %(table_users)s USING BTREE(screen_name);
	CREATE INDEX ON %(table_users)s USING BTREE(id);

	CREATE TABLE IF NOT EXISTS %(table_tweets)s (
		row SERIAL PRIMARY KEY,
		created_at TIMESTAMP WITH TIME ZONE,
		favorited BOOLEAN,
		favorite_count INTEGER,
		id BIGINT,
		in_reply_to_user_id BIGINT,
		in_reply_to_status_id BIGINT,
		lang CHARACTER VARYING (16),
		place CHARACTER VARYING (255),
		retweet_count INTEGER,
		source CHARACTER VARYING (255),
		text CHARACTER VARYING (160),
		truncated BOOLEAN,
		location CHARACTER VARYING (255),
		usr BIGINT,
		urls CHARACTER VARYING (1024) [],
		user_mentions BIGINT[],
		hashtags CHARACTER VARYING (255) [],
		geom geometry(POINT,4326)
	);
	CREATE INDEX ON %(table_tweets)s USING GIST(geom);
	CREATE INDEX ON %(table_tweets)s USING BTREE(id);
""" %({"table_users":table_users,"table_tweets":table_tweets})




def process_tweet(tweet,cursor):
	"""
	inserts a tweet into the database, 
	also creates/updates user record etc
	"""
	try:
		created_at=datetime.fromtimestamp(tweet.created_at_in_seconds) 
		urls=[u.expanded_url[:1023] for u in tweet.urls]
		user_mentions=[u.id for u in tweet.user_mentions]
		hashtags=[h.text[:254] for h in tweet.hashtags]
		try:
			lon=tweet.coordinates['coordinates'][0]
			lat=tweet.coordinates['coordinates'][1]
		except:
			lon=None
			lat=None

		cursor.execute("SELECT count(*)>0 FROM "+table_tweets+" WHERE id=%s", (tweet.id,))
		if cursor.fetchone()[0]>0:
			return # we already saved that tweet at some point, don't produce duplicates on purpose
	
		cursor.execute("SELECT nextval(pg_get_serial_sequence('"+table_tweets+"', 'row'))")
		row=cursor.fetchone()[0]
	
		cursor.execute("\
			INSERT INTO "+table_tweets+" \
			VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, ST_SetSRID(ST_Point(%s, %s),4326))",
			(
				row, 
				created_at, 
				tweet.favorited, 
				tweet.favorite_count, 
				tweet.id, 
				tweet.in_reply_to_user_id, 
				tweet.in_reply_to_status_id, 
				tweet.lang[:15] if tweet.lang is not None else "",
				str(tweet.place)[:254] if tweet.place is not None else "",
				tweet.retweet_count, 
				tweet.source[:254] if tweet.source is not None else "",
				tweet.text[:159] if tweet.text is not None else "",
				tweet.truncated, 
				tweet.location[:254] if tweet.location is not None else "", 
				tweet.user.id, 
				urls, 
				user_mentions, 
				hashtags, 
				lon, lat
			)
		)
	
		cursor.execute("SELECT count(*)>0 FROM "+table_users+" WHERE id=%s", (tweet.user.id,))
		if cursor.fetchone()[0]==0: # user does not exist already -> insert them
			#created_at=datetime.strptime(tweet.user.created_at,"%a %b %d %H:%M:%S %z %Y") # does not work with tz -> http://stackoverflow.com/questions/10494312/parsing-time-string-in-python#10494427
			created_at=parser.parse(tweet.user.created_at)			
			
			cursor.execute("SELECT nextval(pg_get_serial_sequence('"+table_users+"', 'row'))")
			row=cursor.fetchone()[0]
			
			cursor.execute("\
				INSERT INTO "+table_users+" \
				VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
				(
					row, 
					created_at, 
					tweet.user.description[:511] if tweet.user.description is not None else "", 
					tweet.user.favourites_count, 
					tweet.user.followers_count, 
					tweet.user.friends_count, 
					tweet.user.geo_enabled, 
					tweet.user.id, 
					tweet.user.lang[:15] if tweet.user.lang is not None else "", 
					tweet.user.listed_count, 
					tweet.user.location[:254] if tweet.user.location is not None else "", 
					tweet.user.name[:511] if tweet.user.name is not None else "", 
					tweet.user.protected, 
					tweet.user.screen_name[:254] if tweet.user.screen_name is not None else "", 
					tweet.user.statuses_count, 
					tweet.user.time_zone[:63] if tweet.user.time_zone is not None else "", 
					tweet.user.utc_offset
				)
			)

	except Exception,e:
		print(str(e))
		print("Could not insert tweet #%d into DB" %(tweet.id,))
		print(tweet)

def check_rate_limit(api):
	limit_status=api.GetRateLimitStatus()["resources"]["search"]["/search/tweets"]
	while limit_status["remaining"]<20: # stay on the safe side, something fishy happened with this limit before
		until_reset=(datetime.fromtimestamp(limit_status["reset"])-datetime.now()).total_seconds()
		until_reset*=1.0+random() # will this help avoid race conditions with the other monitor-tweets?
		for s in range(int(until_reset/10)*10+10,0,-10):
			print("Rate limit reached, waiting %dsec               " %(s,), end='\r')
			time.sleep(10) 
		limit_status=api.GetRateLimitStatus()["resources"]["search"]["/search/tweets"]

def check_and_search(api, **kwargs):
	try:
		check_rate_limit(api)
		return api.GetSearch(**kwargs)
	except twitter.TwitterError,e:
		print("Twitter coughed up a bit, let's wait a couple of minutes before continuing.") # this seems like a reasonable thing to do if we are within rate limit but get a negative response from the twitter api – the only possible error on our side is a too complex query
		print("\nThe exact error message was as follows:\n%s\n"%(str(e)))
		time.sleep(120)
		return check_and_search(api,**kwargs)
	except requests.exceptions.ConnectionError,e:
		print("Connection timed out – waiting some 15 minutes …")
		time.sleep(900)
		return check_and_search(api,**kwargs)

def main():
	try:
		connection=psycopg2.connect(pg_conn)
		cursor=connection.cursor()
		cursor.execute("SELECT count(*)=2 FROM information_schema.tables WHERE table_name=%s OR table_name=%s", (table_tweets,table_users))
		if not cursor.fetchone()[0]:
			cursor.execute(createsql)
	except:
		print("Database connection could not be established. Exiting.")
		return -1

	try:
		api=twitter.Api(consumer_key=consumer_key, consumer_secret=consumer_secret, access_token_key=access_token_key, access_token_secret=access_token_secret)
	except:
		print("Could not connect to Twitter API. Exiting.")
		return -1

	total=0

	# first, fetch all tweets from the past (going back in time)
	try:
		cursor.execute("SELECT min(id) FROM "+table_tweets)
		oldest_id=cursor.fetchone()[0] # (None,) or (ID,)
		oldest_id=0 if oldest_id is None else oldest_id
	
		results=check_and_search(api,term=searchterm,count=batchsize,result_type="recent",max_id=oldest_id)
		while len(results)>1:
			total+=len(results)
			print("Fetched a total of %d tweets [R]" %(total,),end='\r')

			for tweet in results:
				process_tweet(tweet, cursor)
			connection.commit()
			
			oldest_id=min([t.id for t in results]) # the furthest back
			
			results=check_and_search(api,term=searchterm,count=batchsize,result_type="recent",max_id=oldest_id)

	except KeyboardInterrupt:
		print("\n", end='')
		return
	except:
		print("Fetching tweets from the past retrospectively failed somehow.")
		return -1


	# then, fetch new tweets as they pop up
	while True:
		try:
			total=0 # not a grand total ;-)
			
			cursor.execute("SELECT max(id) FROM "+table_tweets)
			latest_id=cursor.fetchone()[0] 
			
			results=check_and_search(api,term=searchterm,count=batchsize,result_type="recent")
			
			#print("\n".join(["%s\n%s\n%s\n\n"%(r.text,r.id, r.created_at) for r in results]))
			#print(min([t.id for t in results]))
			#print(latest_id)
			
			while len(results)>1 and min([t.id for t in results])>=latest_id: # as long as we can fetch younger tweets than our latest in the db
				total+=len(results)
				print("Fetched %d tweets [L]" %(total,),end='\r')

				for tweet in results:
					process_tweet(tweet, cursor)
				
				results=check_and_search(api,term=searchterm,count=batchsize,result_type="recent",max_id=min([t.id for t in results]))
			
			connection.commit()
			for w in range(waittime*60,0,-10):
				print("Waiting for more tweets to show up at this party (%02d:%02d:%02d/%02d:%02d:00 wait time remaining)" %(w/3600,w/60,w%60,waittime/60,waittime),end='\r')
				time.sleep(10)
						
			
		except psycopg2.OperationalError:  # probably postgresql server restarted
			try:
				connection.rollback()
			except:
				pass
			finally:
				time.sleep(5*60) # wait five minutes, then it should definitely be up again
				connection.close()
				connection=psycopg2.connect(pg_conn)
                		cursor=connection.cursor()
			continue
		except KeyboardInterrupt:
			connection.rollback() # otherwise we might end up with gaps (hard to detect!) -> better re-fetch during next run
			print("\n", end='')
			break


if __name__ == '__main__':
	main()
